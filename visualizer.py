from __future__ import division
import json
import matplotlib.pyplot as plt
import csv
import numpy as np
with open("dummy.json",'r') as f:
    root = json.load(f)
orga_count=None

def person_counter():
    person_dict={}
    for task in root:
        for person in task["students"]:
            if not person in person_dict:
                person_dict[person]=[1,[task["organisation"],1]]
                #print("created: " + str(person) + ":"+ str(person_dict[person]))
            else:
                ulala = True
                for i, j in enumerate(person_dict[person]):
                    if i == 0:
                        continue
                    if task["organisation"] in j:
                        #print(person_dict[person])
                        person_dict[person][i][1]+=1
                        ulala = False
                        break
                if ulala == True:
                    person_dict[person].append([task["organisation"],1])
                person_dict[person][0]+=1
    numero_unos=[]
    for w in sorted(person_dict, key=person_dict.get):
        orga=False
        orga_name = "Apertium"
        skipper = True
        printi = False
        if len(person_dict[w]) > 2:
            #print(len(person_dict[w]))
            #print(person_dict[w])
            no1 = [None,0]
            no2=[None,0]
            for i,j in enumerate(person_dict[w]):
                if i == 0:
                    continue
                if j[1] > no1[1]:
                    no1 = [j[0],j[1]]
                if j[1] > no2[1] and j[0] != no1[0]:
                    no2 = [j[0],j[1]]
            if no1[0] == orga_name or True == skipper:
                numero_unos.append(no1)
                if printi:
                    print(str(w)+ "  " + str(person_dict[w][0]) + " most at: " + str(no1) + " second at: " + str(no2))
        else:            
            if person_dict[w][1][0] == orga_name or True==skipper:
                numero_unos.append(person_dict[w][1][1])
                if printi:
                    print(str(w)+ "  " + str(person_dict[w][0]) +" at: " + str(person_dict[w][1][0]))
    average_pers_counter(numero_unos)
    print(person_dict["dolphingarlic"])
def organisation_counter():
    orgas={}
    global orga_count
    for task in root:
        if not task["organisation"] in orgas:
            orgas[task["organisation"]] = 1
        else:
            orgas[task["organisation"]]+=1
    for w in sorted(orgas, key=orgas.get):

        print(str(w) +":" +str(orgas[w]) )
    orga_count = orgas


def average_pers_counter(numero_unos):
    orgas={}
    #print(numero_unos)
    for no1 in numero_unos:
        #print(no1)
        if not no1[0] in orgas:
            #print(no1[0])
            orgas[no1[0]] = [no1[1],1]
            #print(orgas)
        else:
            orgas[no1[0]][0]+=no1[1]
            orgas[no1[0]][1]+=1
    for orga in orgas:
        orgas[orga]=(orgas[orga][0])/orgas[orga][1]
    for w in sorted(orgas, key=orgas.get):
        #print(orgas.get())
        #print(w)
        #print(orgas[w])
        print(str(w) +":" +str(orgas[w]))
    #plot_both(orgas,orga_count)
    #with open("task_per_orga.csv",'w') as outi:
        #w = csv.DictWriter(outi, orgas.keys())
        #w.writeheader()
        #w.writerow(orgas)
def fav_counter():
    person_dict={}
    orgas={}
    for task in root:
        if not task["organisation"] in orgas:
            orgas[task["organisation"]] = 1
        else:
            continue
    for task in root:
        for person in task["students"]:
            if not person in person_dict:
                person_dict[person]=[1,[task["organisation"],1]]
                #print("created: " + str(person) + ":"+ str(person_dict[person]))
            else:
                ulala = True
                for i, j in enumerate(person_dict[person]):
                    if i == 0:
                        continue
                    if task["organisation"] in j:
                        #print(person_dict[person])
                        person_dict[person][i][1]+=1
                        ulala = False
                        break
                if ulala == True:
                    person_dict[person].append([task["organisation"],1])
                person_dict[person][0]+=1
    orga_top={}
    for orga_name in orgas.keys():
        skipper = False
        no1=[None,0]
        no2=[None,0]
        for w in person_dict:
            if len(person_dict[w]) > 2:
                #print(len(person_dict[w]))
                #print(person_dict[w])
                for i,j in enumerate(person_dict[w]):
                    if i == 0:
                        continue
                    if j[0] == orga_name:
                        if j[1] > no1[1]:
                            no1 = [w,j[1]]
                        if j[1] > no2[1] and w != no1[0]:
                            no2 = [w,j[1]]
            else:
                if person_dict[w][1][0] == orga_name:
                    if person_dict[w][1][1] > no1[1]:
                        no1=[w,person_dict[w][1][1]]
                    if person_dict[w][1][1] > no2[1] and w != no1[0]:
                        no2=[w,person_dict[w][1][1]]
        orga_top[orga_name] = (no1,no2)
    winner={}
    for orga in orga_top:
        winner[orga]=orga_top[orga][0][0]
    #for orga in orga_top: #both winners
        #orga_top[orga]=(orga_top[orga][0][1]+orga_top[orga][1][1])/2
    for orga in orga_top: #best one
        orga_top[orga]=orga_top[orga][0][1]
    plot_fav(orga_top,orga_count,winner)
        #average_pers_counter(numero_unos)

def funci(x):
    return x[1]
def plot_tasks(plot_data):
    lists = sorted(plot_data.items(),key=funci)
    x,y = zip(*lists)
    plt.xticks(rotation=45)
    plt.plot(x, y)
    plt.show()

def plot_both(orga1,orga2):
    list1 = sorted(orga1.items(),key=funci)
    list1_help = [i[0] for i in list1]
    list2 = sorted(orga2.items(),key=lambda orga : list1_help.index(orga[0]))
    x,y1 = zip(*list1)
    x,y2 = zip(*list2)
    list_ratio = [(list1[i][0],list1[i][1]/list2[i][1]) for i in range(len(list1))]
    x, y3 = zip(*list_ratio)
    width = 0.35   
    ind=np.arange(len(list1))
    fig, ax = plt.subplots()
    rects1=ax.bar(ind,y1, width,label="bearbeitete Tasks")
    rect2 = ax.bar(ind+width,y2, width, label="gestellte Tasks")
    ax.set_xticks(ind+width/2)
    ax.set_xticklabels(x,rotation=90)
    axes2 = plt.twinx()
    axes2.plot(x, y3, color='k', label='Ratio')
    axes2.set_ylabel('Verhältnis')
    ax.set_ylabel('Anzahl an Tasks')
    plt.title('Codein 2018')
    ax.legend(loc='best')
    plt.show()
def plot_fav(orga1,orga2,winner):
    list1 = sorted(orga1.items(),key=funci)
    list1_help = [i[0] for i in list1]
    list2 = sorted(orga2.items(),key=lambda orga : list1_help.index(orga[0]))
    x,y1 = zip(*list1)
    x,y2 = zip(*list2)
    list_ratio = [(list1[i][0],list1[i][1]/list2[i][1]) for i in range(len(list1))]
    x, y3 = zip(*list_ratio)
    lst = list(x)
    #x["AOSSIE - Australian Open Source Software Innovation and Education"] = "AOSSIE"
    for name in lst:
        lst[lst.index(name)] = name + ":\n" + winner[name]
    x=tuple(lst)
    width = 0.35   
    ind=np.arange(len(list1))
    fig, ax = plt.subplots()
    rects1=ax.bar(ind,y1, width,label="bearbeitete Tasks des Siegers")
    rect2 = ax.bar(ind+width,y2, width, label="gestellte Tasks")
    ax.set_xticks(ind+width/2)
    ax.set_xticklabels(x,rotation=90)
    axes2 = plt.twinx()
    axes2.plot(x, y3, color='k', label='Ratio')
    axes2.set_ylabel('Verhältnis')
    ax.set_ylabel('Anzahl an Tasks')
    plt.title('Codein 2018')
    ax.legend(loc='best')
    plt.show()

organisation_counter()
#person_counter()
fav_counter()