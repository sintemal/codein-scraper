from bs4 import BeautifulSoup
import requests
import re
import json
import time
start_url = raw_input("what website to crawl?")
pages = input("how many pages to scrape?")
task_stash=[]
task_counter = 0
a=0
for i in range(1,pages+1):
    while True:
        print("currently on page "+str(i))
        if "?page" in start_url:
            url = start_url[:-1] + str(i)
        else:
            url = start_url + "?page=" + str(i)
        response = requests.get(url)
        content = BeautifulSoup(response.content, "html.parser")
        try:
            time.sleep(0.2)
            for url in content.findAll('a', attrs={"class": "md-button md-primary md-raised md-codein-theme"}):
                task = requests.get("https://codein.withgoogle.com/"+str(url.get("href")))
                task_content = BeautifulSoup(task.content, "html.parser")
                organisation = task_content.find('div', attrs={"class": "md-subhead task-definition__organization"}).text
                tags = []
                for tag in task_content.findAll('li', attrs={"class": "task-definition__tag"}):
                    tags.append(tag.text.encode('utf-8'))
                students_prev = task_content.find('h4', attrs={"class": "task-definition__subheader task-definition__students-subheader"})
                students = students_prev.find_next("div").text.split(",")
                categories = []
                for category in task_content.findAll('span', attrs={"class": "task-category__name"}):
                    categories.append(category.text.encode('utf-8'))
                title = task_content.find('h3', attrs={"class": "md-headline task-definition__name"}).text
                taskObject = {
                    "organisation" : organisation,
                    "title" : title,
                    "tags" : tags,
                    "students" : students,
                    "categories" : categories
                }
                task_stash.append(taskObject)
                task_counter+=1
                if (task_counter%10) == 0:
                    print("already parsed " + str(task_counter) + " tasks")
        except:
            print(task_content)
            mail = task_content.find('p').text
            sec = re.findall("\d+\.\d+",mail)
            sec = float(sec[0])
            print("sleeping "+str(sec)+ " seconds now")
            time.sleep(sec+3)
            continue
        break
    if (i%10==0):
        with open("task"+str(a)+".json","w") as outfile:
            json.dump(task_stash, outfile)
            a+=1
            task_stash=[]
with open("task"+str(a)+".json","w") as outfile:
            json.dump(task_stash, outfile)