import json
for i in range(1,269):
    root = "dummy.json"
    print(i)
    if i == 1:
        continue
    if i == 2:
        root = "../data/task1.json"
    with open(root,'r') as root:
        rooty = json.load(root)
    
    with open("../data/task"+str(i)+".json",'r') as tail:
        taily = json.load(tail)

    rooty = rooty + taily

    with open("dummy.json",'w') as out:
        json.dump(rooty,out)
    out.close()
    tail.close()
    root.close()